/**
 * @file test_event_listeners.cpp
 * Test script for SDLViewer event listeners
 *
 * @brief launches an SDLViewer to visualize the system
 * 
 * 	Adds events to the viewer available events are :
 *		MouseDown
 *		RightClick/LeftClick
 *		KeyDown
 *		LongClick
 */

#include <fstream>

#include "SDLViewer.hpp"
#include "Util.hpp"

typedef typename CS207::SDLViewer Viewer;

struct right_click : public Viewer::EventListener {
  void operator()() {
    std::cout << "Right Click!"<< std::endl;
  }
};

struct test_key : public Viewer::EventListener {
  void operator()(const char* key) {
    std::cout << key<< std::endl;
  }
};

struct test_longclick : public Viewer::EventListener {
  void operator()(double duration) {
    std::cout << "Left Click!	"<< duration<< std::endl;
  }
};

int main()
{
  // Launch a viewer
  CS207::SDLViewer viewer;
  viewer.launch();

  // ADD EVENT HANDLERS
  right_click right=right_click();
  viewer.add_listener("RightClick",&right);	//test rightlcick events
  test_key key=test_key();
  viewer.add_listener("KeyDown",&key);		//test keyboard events
  test_longclick longclick=test_longclick();
  viewer.add_listener("LongClick",&longclick);	//test longclick event
  test_longclick err=test_longclick();
  viewer.add_listener("Err",&err);		//test invalid event

  return 0;
}
