**SDL Viewer Extension**
======================
*Yoonji Choe and Muhammad Ibrahim*
--------------------------------

You can use our SDL Viewer class to do anything when the following events happen:

- Right click
- Left Click (including duration of click)
- Press any key on keyboard

We have provided an example on how to use the viewer in `test_event_listeners.cpp`. Run `make test_event_listeners` to run the example. It logs the events on the terminal as an example but you can modify the code to make it call custom functions.

To get started follow these steps:

- Replace your SDLViewer class with ours
- Add an event listner functor, inheriting from our SDLViewer's EventListner class, to define what you want to happen when the event occurs, making sure to pass in `double` param for "LongClick" and `const char*` for "KeyDown". For example:

`
struct test_click : public Viewer::EventListener {
  void operator()() {
    std::cout << "Click!"<< std::endl;
  }
};
`

- Call the add_listener method of the viewer, `add_listener(firstparam, secondparam)`

`firstparam` is "LeftClick", "RightClick", "KeyDown", "LongClick" and "MouseDown" depending on which for which event you are adding the listener. `secondparam` is the functor that you want to run when the event occurs.
